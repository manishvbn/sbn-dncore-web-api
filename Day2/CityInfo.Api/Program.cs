using CityInfo.Api;
using Microsoft.AspNetCore.StaticFiles;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

// builder.Services.AddControllers();

// builder.Services.AddControllers(options=>{
//     options.ReturnHttpNotAcceptable = true;
// });

builder.Services.AddControllers(options=>{
    options.ReturnHttpNotAcceptable = true;
}).AddNewtonsoftJson().AddXmlDataContractSerializerFormatters();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSingleton<FileExtensionContentTypeProvider>();
builder.Services.AddSingleton<CitiesDataStore>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    //app.Run(async context =>
    //{
    //    await context.Response.WriteAsync("Hello World!");
    //});

    //app.Run(async context =>
    //{
    //    await context.Response.WriteAsync("Hello World Again!");
    //});

    //app.Use(async (context, next) =>
    //{
    //    await context.Response.WriteAsync($"Middleware One - Request {Environment.NewLine}");
    //    await next();
    //    await context.Response.WriteAsync($"Middleware One - Response {Environment.NewLine}");
    //});

    //app.Use(async (context, next) =>
    //{
    //    await context.Response.WriteAsync($"Middleware Two - Request {Environment.NewLine}");
    //    await next();
    //    await context.Response.WriteAsync($"Middleware Two - Response {Environment.NewLine}");
    //});

    //app.Run(async context =>
    //{
    //    await context.Response.WriteAsync($"Hello World! {Environment.NewLine}");
    //});

    app.Map("/branchone", MapBranchOne);
    app.Map("/branchtwo", MapBranchTwo);

    app.UseSwagger();
    app.UseSwaggerUI();
}

static void MapBranchOne(IApplicationBuilder app)
{
    app.Run(async context =>
    {
        await context.Response.WriteAsync($"You are on Branch One!");
    });
}

static void MapBranchTwo(IApplicationBuilder app)
{
    app.Run(async context =>
    {
        await context.Response.WriteAsync($"You are on Branch Two!");
    });
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
