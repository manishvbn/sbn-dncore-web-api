﻿public class Identity
{
    private static int idGenerator = 0;
    public int EmpId { get; private set; }
    public string Name { get; set; }
    public string SurName { get; set; }

    public Identity(string name, string surName)
    {
        EmpId = ++idGenerator;
        Name = name;
        SurName = surName;
    }
}

public interface IEmployee {
    Identity Identity { get; set; }

}
public class Employee : IEmployee
{
    public Identity Identity { get; set; }

    public Employee(string name, string surName)
    {
        Identity = new Identity(name, surName);
    }
}

public class Project
{
    public IEmployee Manager { get; set; }

    public Project(IEmployee employee)
    {
        Manager = employee;
    }
}

public class Program
{
    public static void Main(string[] args)
    {
        var john = new Employee("John", "Doe");
        var project = new Project(john);
        Console.WriteLine($"Employee ID: {project.Manager.Identity.EmpId}");
        project = null;
        Console.WriteLine($"Employee ID: {john.Identity.EmpId}");
    }
}