// using Microsoft.AspNetCore.Mvc;

// [ApiController]
// [Route("api/cities")]
// public class CitiesController : ControllerBase
// {
//     [HttpGet]
//     public JsonResult GetCities() {
//         return new JsonResult(new [] {
//             new { id = 1, Name = "New York City" },
//             new { id = 2, Name = "Antwerp" }
//         });
//     }

//     [HttpGet("{id}")]
//     public JsonResult GetCity(int id) {
//         var data = new [] {
//             new { id = 1, Name = "New York City" },
//             new { id = 2, Name = "Antwerp" }
//         };

//         return new JsonResult(data.FirstOrDefault(x => x.id == id));
//     }
// }

// ------------------------------------------

using CityInfo.Api;
using CityInfo.Api.Models;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/cities")]
public class CitiesController : ControllerBase
{
    // [HttpGet]
    // public JsonResult GetCities()
    // {
    //     return new JsonResult(CitiesDataStore.Current.Cities);
    // }

    // [HttpGet("{id}")]
    // public JsonResult GetCity(int id)
    // {
    //     return new JsonResult(CitiesDataStore.Current.Cities
    //             .FirstOrDefault(c => c.Id == id));
    // }

    // [HttpGet]
    // public IEnumerable<CityDto> GetCities()
    // {
    //     return CitiesDataStore.Current.Cities;
    // }

    // [HttpGet("{id}")]
    // public CityDto GetCity(int id)
    // {
    //     return CitiesDataStore.Current.Cities.FirstOrDefault(c => c.Id == id);
    // }

    // [HttpGet]
    // [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<CityDto>))]
    // public IActionResult GetCities()
    // {
    //     return Ok(CitiesDataStore.Current.Cities);
    // }

    // [HttpGet("{id}")]
    // [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CityDto))]
    // [ProducesResponseType(StatusCodes.Status404NotFound)]
    // public IActionResult GetCity(int id)
    // {
    //     var city = CitiesDataStore.Current.Cities.FirstOrDefault(c => c.Id == id);
    //     return city == null ? NotFound() : Ok(city);
    // }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public ActionResult<IEnumerable<CityDto>> GetCities()
    {
        return Ok(CitiesDataStore.Current.Cities);
    }

    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public ActionResult<CityDto> GetCity(int id)
    {
        var city = CitiesDataStore.Current.Cities.FirstOrDefault(c => c.Id == id);
        return city == null ? NotFound() : Ok(city);
    }
}